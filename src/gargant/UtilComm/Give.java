package gargant.UtilComm;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Give implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender Sender, Command Cmd, String label, String[] args) {
		if (Cmd.getName().equalsIgnoreCase("give")) {
			if (Sender.hasPermission("util.give")) {
				if (args.length == 1) {
					Player Executor = (Player) Sender;
					ItemStack toGive = new ItemStack(Material.getMaterial(args[0].toUpperCase()), 1);
					Executor.getInventory().addItem(toGive);
					return true;
				} else if (args.length == 2) {
					if (!StringUtils.isNumericSpace(args[1])) {
						Player Receiver = (Player) Bukkit.getPlayer(args[0]);
						ItemStack toGive = new ItemStack(Material.getMaterial(args[1].toUpperCase()), 1);
						Receiver.getInventory().addItem(toGive);
						return true;
					} else {
						Player Executor = (Player) Sender;
						ItemStack toGive = new ItemStack(Material.getMaterial(args[0].toUpperCase()),
								Integer.parseInt(args[1]));
						Executor.getInventory().addItem(toGive);
						return true;
					}
				} else if (args.length == 3) {
					Player Receiver = (Player) Bukkit.getPlayer(args[0]);
					ItemStack toGive = new ItemStack(Material.getMaterial(args[1].toUpperCase()),
							Integer.parseInt(args[2]));
					Receiver.getInventory().addItem(toGive);
					return true;
				} else {
					Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cIncorect usage of the command!"));
				}
				return true;
			}
		}
		return false;
	}

}
