package gargant.UtilComm;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Heal implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender Sender, Command Cmd, String label, String[] args) {
		if (Cmd.getName().equalsIgnoreCase("heal")) {
			if (Sender.hasPermission("util.heal")) {
				if (args.length == 0) {
					Player Executor = (Player) Sender;
					Executor.setHealth(20);
					Executor.setFoodLevel(20);
					Executor.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aHealed!"));
					return true;
				} else if (args.length == 1) {
					Player Other = Bukkit.getPlayer(args[0]);
					Other.setHealth(20);
					Other.setFoodLevel(20);
					Other.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aHealed!"));
					Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aHealed!"));
					return true;
				} else if (args.length == 2) {
					Player Other = Bukkit.getPlayer(args[0]);
					if (StringUtils.isNumericSpace(args[1])) {
						double toAdd = Integer.parseInt(args[1]);
						Other.setHealth(toAdd + Other.getHealth());
						Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aHealed!"));
						return true;
					}
				} else {
					Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cInvalid usage of the command!"));
				}
			}

		}
		return false;
	}
}
