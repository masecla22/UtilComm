package gargant.UtilComm;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Teleport implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender Sender, Command Cmd, String label, String[] args) {
		if (Cmd.getName().equalsIgnoreCase("tp") && Sender.hasPermission("util.tp")) {
			if (args.length == 1) {
				Player Telep = (Player) Bukkit.getPlayer(args[0]);
				Player Executor = (Player) Sender;
				if(Telep == null)
					Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlayer "+args[0]+" isn't online!"));
				else{
				Location Loc = (Location) Telep.getLocation();
				Executor.teleport(Loc);
				}			}
			if (args.length == 2) {
				Player Telep = (Player) Bukkit.getPlayer(args[0]);
				Player Where = (Player) Bukkit.getPlayer(args[1]);
				if(Telep != null && Where != null){
				Location Loc = (Location) Where.getLocation();
				Telep.teleport(Loc);
				}else{
					if(Telep == null)
						Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlayer "+args[0]+" isn't online!"));
					if(Where == null)
						Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlayer "+args[1]+" isn't online!"));
				}
			}
			if(args.length == 3){
				if(StringUtils.isNumericSpace(args[0]) && StringUtils.isNumericSpace(args[1]) && StringUtils.isNumericSpace(args[2])){
					Player player = (Player) Sender;
					player.teleport(new Location(player.getWorld(),Integer.parseInt(args[0]),Integer.parseInt(args[1]),Integer.parseInt(args[2])));
				}else{
					Sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cSomething went wrong!"));
				}
			}
			return true;
		}

		return false;
	}
}
