package gargant.UtilComm;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;

public class Hat implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("hat")) {
			if (args.length != 0) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cWrong usage!"));
			}
			else{
				Player who = (Player) sender;
				if(who.getInventory().getItemInMainHand().getType().equals(Material.AIR)){
					if(who.getInventory().getHelmet().getType().equals(Material.AIR)){
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou don't have any item in your hand!"));
					}
					else{						
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUnequipped your hat!"));
						who.getInventory().setItemInMainHand(who.getInventory().getHelmet());
						who.getInventory().setHelmet(new ItemStack(Material.AIR,1));
					}
				}
				else{
					if(who.getInventory().getHelmet()==null){
						who.getInventory().setHelmet(who.getInventory().getItemInMainHand());
						who.getInventory().setItemInMainHand(new ItemStack(Material.AIR,1));
					}
					else{
						ItemStack ff = who.getInventory().getHelmet();
						who.getInventory().setHelmet(who.getInventory().getItemInMainHand());
						who.getInventory().setItemInMainHand(ff);
					}
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aEquipped your hat!"));
				}
				
			}
			return true;
		}
		return false;
	}

}
