package gargant.UtilComm;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Fly implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender Sender, Command Cmd, String label, String[] args) {
		if (Cmd.getName().equalsIgnoreCase("fly")) {
			Player Executor = (Player) Sender;
			if (Sender.hasPermission("util.fly")) {
				if (Executor.getGameMode() != GameMode.CREATIVE && Executor.getGameMode() != GameMode.SPECTATOR) {
					if (Executor.getAllowFlight()) {
						Executor.setAllowFlight(false);
						Executor.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aFly Disabled"));
						return true;
					} else {
						Executor.setAllowFlight(true);
						Executor.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aFly Enabled"));
						return true;
					}
				} else {
					Executor.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou cannot stop flying in your gamemode!"));
					return true;
				}
			}
		}
		return false;
	}

}
